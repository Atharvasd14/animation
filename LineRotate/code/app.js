
var canvas = document.getElementById("mycanvas");
var context = canvas.getContext("2d");
var l1;
function start () {
    var p1 = new point(300,300);
    var p2 = new point(400,300);
    l1 = new line(p1,p2,context);
    animation();
}
function animation () {
    context.clearRect(0,0,canvas.width,canvas.height);
    l1.draw();
    l1.updateangle();
    window.requestAnimationFrame(animation);
}