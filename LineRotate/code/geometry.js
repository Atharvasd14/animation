
class point 
    {
        x;
        y;
        constructor (x ,y )
        {
            this.x=x;
            this.y=y;
        }
    }
     class line 
    {
        p1
        p2;
        angle;
        length;
        context ;
        constructor (p1 , p2,context = CanvasRenderingContext2D)
        {
            this.p1=p1;
            this.p2=p2;
            this.angle = 0;
            this.context = context;
        }
        draw() {
            //alert (this.p2.x);
            this.context.beginPath();
            this.context.moveTo(this.p1.x,this.p1.y);
            this.context.lineTo(this.p2.x,this.p2.y);
            this.context.lineWidth=3;
            this.context.stroke();
        }
        updateangle ()
        {
            this.angle++;
            this.getlength();

            this.p2.x = this.p1.x + this.length*Math.cos(this.angle*Math.PI/180);
            this.p2.y = this.p1.y + this.length*Math.sin(this.angle*Math.PI/180);
        }
        getlength() {
            this.length = Math.pow((this.p2.x-this.p1.x),2)+Math.pow((this.p2.y-this.p1.y),2);
            
            this.length = Math.sqrt(this.length);
           // alert (this.length);
        
        }
    }
