namespace Geometry {
    export class point 
    {
        public x:number;
        public y:number;
        constructor (x :number ,y: number )
        {
            this.x=x;
            this.y=y;
        }
    }
    export class Rectangle {
        private _l: number;
        private _w: number;
        private _stpt: point;
        private xmin:number;
        private xmax:number;
        private ymin:number;
        private ymax:number;
        public color: string;
        public stxcord: number;
        public stycord: number;
        public context;
        public s:number;
        public t:number;
        private left : boolean; 
        private right : boolean; 
        private up : boolean; 
        private down : boolean; 
        constructor(context: CanvasRenderingContext2D ,l: number, w: number, stpt: point, color?: string) {
            this._l = l;
            this._w = w;
            this._stpt = stpt;
            this.context = context;
            this.stxcord=this._stpt.x;
            this.stycord=this._stpt.y;
            this.s=1;
            this.t=1;
            this.xmin=this._stpt.x;
            this.ymin=this._stpt.y;
            this.xmax=this.xmin+300;
            this.ymax=this.ymin+200;
            this.left = true;
            this.right = false;
            this.up = false;
            this.down = false;
            if (typeof color != 'undefined')
            {
                this.color = color;
            }
            else {
                this.color = "red";
            }
        }

        draw()
        {
            this.context.beginPath();
            this.context.rect(this._stpt.x, this._stpt.y, this._l, this._w);
            this.context.lineWidth = 3;
            this.context.fillStyle=this.color;
            this.context.fill();
            this.context.stroke();
        }
        tofromotion (tofrolength: number)  {
            this.draw();
            if (this.left)
            {
                if (this._stpt.x!=this.xmax)
                {
                    this._stpt.x++;
                }
                else {
                    this.left=false;
                    this.down=true;
                }
            }
            if (this.right)
                {
                    if (this._stpt.x!=this.xmin)
                    {
                        this._stpt.x--;
                    }
                    else
                    {
                        this.right=false;
                        this.up = true;
                    }
                }
            if (this.up)
                {
                    if (this._stpt.y!=this.ymin)
                    {
                        this._stpt.y--;
                    }
                    else
                    {
                        this.up = false;
                        this.left = true;
                    }
                }
            if (this.down)
                {
                    if (this._stpt.y!=this.ymax)
                    {
                        this._stpt.y++;
                    }
                    else{
                        this.down = false;
                        this.right=true;
                    }
                }
           
        }
        // leftmotion (tofrolength: number)  {
        //     this._stpt.x++;
            
        // }
        // downmotion (tofrolength: number)  {
            
        //     this._stpt.y++;
        // }
        // rightmotion (tofrolength: number)  {
        //     this._stpt.x--;
            
        // }
        // upmotion (tofrolength: number)  {
           
        //    this._stpt.y--;
        // }
    }
    

}