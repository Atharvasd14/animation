"use strict";
var Geometry;
(function (Geometry) {
    var point = /** @class */ (function () {
        function point(x, y) {
            this.x = x;
            this.y = y;
        }
        return point;
    }());
    Geometry.point = point;
    var Rectangle = /** @class */ (function () {
        function Rectangle(context, l, w, stpt, color) {
            this._l = l;
            this._w = w;
            this._stpt = stpt;
            this.context = context;
            this.stxcord = this._stpt.x;
            this.stycord = this._stpt.y;
            this.s = 1;
            this.t = 1;
            this.xmin = this._stpt.x;
            this.ymin = this._stpt.y;
            this.xmax = this.xmin + 300;
            this.ymax = this.ymin + 200;
            this.left = true;
            this.right = false;
            this.up = false;
            this.down = false;
            if (typeof color != 'undefined') {
                this.color = color;
            }
            else {
                this.color = "red";
            }
        }
        Rectangle.prototype.draw = function () {
            this.context.beginPath();
            this.context.rect(this._stpt.x, this._stpt.y, this._l, this._w);
            this.context.lineWidth = 3;
            this.context.fillStyle = this.color;
            this.context.fill();
            this.context.stroke();
        };
        Rectangle.prototype.tofromotion = function (tofrolength) {
            this.draw();
            if (this.left) {
                if (this._stpt.x != this.xmax) {
                    this._stpt.x++;
                }
                else {
                    this.left = false;
                    this.down = true;
                }
            }
            if (this.right) {
                if (this._stpt.x != this.xmin) {
                    this._stpt.x--;
                }
                else {
                    this.right = false;
                    this.up = true;
                }
            }
            if (this.up) {
                if (this._stpt.y != this.ymin) {
                    this._stpt.y--;
                }
                else {
                    this.up = false;
                    this.left = true;
                }
            }
            if (this.down) {
                if (this._stpt.y != this.ymax) {
                    this._stpt.y++;
                }
                else {
                    this.down = false;
                    this.right = true;
                }
            }
        };
        return Rectangle;
    }());
    Geometry.Rectangle = Rectangle;
})(Geometry || (Geometry = {}));
