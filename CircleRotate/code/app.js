var canvas = document.getElementById("mycanvas");
var context = canvas.getContext("2d");
var Mycircle;
var angle = 0;
var hasstarted = 0;
var centrex,centrey,arbtptx,arbtpty,radius;
centrex=parseInt(document.getElementById("centrex").value);    
centrey=parseInt(document.getElementById("centrey").value);
arbtptx=parseInt(document.getElementById("arbtptx").value);
arbtpty=parseInt(document.getElementById("arbtpty").value);
radius=parseInt(document.getElementById("radius").value);
function start () {
    
    centre = new point(centrex,centrey);
    arbtpt = new point(arbtptx,arbtpty);
    Mycircle = new circle(centre,arbtpt,radius,context);
    Mycircle.angle=angle;
    hasstarted = 1;
    animation();
}
function stoprotate()
{
    hasstarted = 0;
    angle=Mycircle.angle;
}
function animation () {
    context.clearRect(0,0,canvas.width,canvas.height);
    Mycircle.draw();
    Mycircle.updateangle();
    if (hasstarted)
        window.requestAnimationFrame(animation);
}