
class point 
    {
        x;
        y;
        constructor (x ,y )
        {
            this.x=x;
            this.y=y;
        }
    }
     class line 
    {
        p1
        p2;
        angle;
        length;
        context;
        color;
        constructor (p1 , p2,context = CanvasRenderingContext2D)
        {
            this.p1=p1;
            this.p2=p2;
            this.angle = 0;
            this.context = context;
            this.color = black;
        }
        draw() {
            //alert (this.p2.x);
            this.context.beginPath();
            this.context.moveTo(this.p1.x,this.p1.y);
            this.context.lineTo(this.p2.x,this.p2.y);
            this.context.lineWidth=3;
            this.context.stroke();
        }
       
        updateangle ()
        {
            this.angle++;
            this.getlength();
            this.p2.x = this.p1.x + this.length*Math.cos(this.angle*Math.PI/180);
            this.p2.y = this.p1.y + this.length*Math.sin(this.angle*Math.PI/180);
        }
        getlength() {
            this.length = Math.pow((this.p2.x-this.p1.x),2)+Math.pow((this.p2.y-this.p1.y),2);
            
            this.length = Math.sqrt(this.length);
        
        
        }
    }
    class circle {
        centre;
        arbtpt;
        r;
        angle;
        length;
        context;
        color;
        constructor (centre , arbtpt , r , context = CanvasRenderingContext2D)
        {
            this.centre=centre;
           
            this.arbtpt=arbtpt;
            this.r=r;
            this.getangle();
            this.context = context;
            this.color = "black";
            this.getlength();
        }
        getangle ()
        {
            this.angle=Math.atan((this.centre.y-this.arbtpt.y)/(this.centre.x-this.arbtpt.x));
            this.angle=this.angle*180/Math.PI;
            
        }
        draw() {
            
            this.context.beginPath();
            this.context.arc(this.centre.x, this.centre.y, this.r, 0, 2 * Math.PI, false);
            this.context.lineWidth=3;
            this.context.color="red";
            this.context.stroke();
    
        }
        updateangle ()
        {
          
            this.angle++;
            this.centre.x = this.arbtpt.x + this.length*Math.cos(this.angle*Math.PI/180);
            this.centre.y = this.arbtpt.y + this.length*Math.sin(this.angle*Math.PI/180);
            
        }
        getlength() {
            this.length = Math.pow((this.arbtpt.x-this.centre.x),2)+Math.pow((this.arbtpt.y-this.centre.y),2);
            
            this.length = Math.sqrt(this.length);

        
        }
    
    }