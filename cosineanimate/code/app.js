var canvas = document.getElementById("mycanvas");
var context = canvas.getContext("2d");
var Mycircle;

var hasstarted = 0;
var isset=false;
var centrex,centrey,arbtptx,arbtpty,radius;
centrex=100;    
centrey=100;
radius=20;
var isforward = true;
function drawaxis()
{

    context.beginPath();
    context.moveTo(100, 100);
    context.lineTo(100, 300);
    context.moveTo(100, 162.5);
    context.lineTo(550, 162.5);
    context.lineWidth = 3;
    context.strokeStyle = "black";
    context.stroke();

}
function start () {
    drawaxis();
    if (!isset)
    {
        isset = true;
        centre = new point(centrex,centrey);
        
        Mycircle = new circle(centre,radius,context);
     
    }
    hasstarted = 1;
    animation();
}
function stoprotate()
{
    hasstarted = 0;
}
function animation () {
    context.clearRect(0,0,canvas.width,canvas.height);
    drawaxis();
    
    if (isforward)
    {
        Mycircle.updateangle();
        Mycircle.draw();
            if (hasstarted)    
                window.requestAnimationFrame(animation);
        if (centre.x==550)
            isforward=false;
    }
    else
    {
        Mycircle.updateangle2();
        Mycircle.draw();
        if (hasstarted)    
            window.requestAnimationFrame(animation);
        if (centre.x==centrex)
            isforward=true;
    }
}